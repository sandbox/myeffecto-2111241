<?php
/**
 * @file
 * Administration forms for the myeffecto module.
 */

require_once 'myeffecto.php';

/**
 * Menu callback; Displays the administration settings for myeffecto.
 */
function myeffecto_admin_settings() {
  global $user;
  $user_id = $user->uid;
  $data    = NULL;
  if (isset($_POST['dataToSend'])) {
    $data = $_POST['dataToSend'];
  }
  $eff_shortname = NULL;
  if (isset($_POST['eff_shortname'])) {
    $eff_shortname = $_POST['eff_shortname'];
  }
  $post_url = NULL;
  if (isset($_GET['postURL'])) {
    $post_url = $_GET['postURL'];
  }
  ?>
  <style type="text/css" media="screen">
		#load {
			background: url(" <?php echo drupal_get_path('module', 'Myeffecto') . '/loading.gif'; ?> ") no-repeat scroll center center #FFF;
			bottom: 0;
			left: 0;
			position: absolute;
			opacity: 0.63;
			right: 0;
			top: 0;
			width: 100%;
			z-index: 1000;
		}
  </style>
  <form id="submitForm" action="" method="post" style="display:none;">
		<input name="isToInsert" value="true" id="isToInsert" type="hidden"/>
		<input name="dataToSend" id="dataToSend" type="hidden"/>
		<input name="eff_shortname" id="eff_shortname" type="hidden"/>
		<input type="submit" />
  </form>
  <form id="reloadForm" action="" method="post" style="display:none;"><input type="submit"/></form><div class="wrap" style="overflow-x : 	hidden; position : relative;">
  <?php
  if (isset($data) && !empty($data)) {
    myeffecto_insertInDb($user_id, NULL, $data, 0, $eff_shortname);
    $form              = array();
    $form['myeffecto'] = array(
      '#type' => 'fieldset',
      '#title' => t('Myeffecto'),
      '#group' => 'settings',
      '#description' => t('<h1 style="text-align:center;">Your emoticon set has been added on your posts successfully.<br /><br />Go to your post to see the effect.</h1>'),
    );
    $types = node_type_get_types();
    return system_settings_form($form);
  }
  else {
    ?>
    <div class="wrap" style="overflow-x : hidden;">
  <?php
    global $_myeffecto_embed_code;
    $_myeffecto_embed_code = myeffecto_get_myeffecto_embed_codeByPostID(0);
    if ((isset($_myeffecto_embed_code) && !empty($_myeffecto_embed_code)) && !isset($post_url)) {
      $form              = array();
      $form['myeffecto'] = array(
        '#type' => 'fieldset',
        '#title' => t('Myeffecto'),
        '#group' => 'settings',
        '#description' => t(myeffecto_allSetCode($_myeffecto_embed_code, NULL)),
      );
      $types = node_type_get_types();
      return system_settings_form($form);
    }
    else {
      $form              = array();
      $form['myeffecto'] = array(
        '#type' => 'fieldset',
        '#title' => t('Myeffecto'),
        '#group' => 'settings',
        '#description' => t(myeffecto_echoFirstUserScript()),
      );
      $types             = node_type_get_types();
      return system_settings_form($form);
    }
  }
  ?>
		</div>
  <?php
}

/**
 * Menu callback; Automatically closes the window after the user logs in.
 */
function myeffecto_closewindow() {
  drupal_add_js('window.close();', 'inline');
  return t('Thank you for logging in. Please close this window, or <a href="@clickhere">click here</a> to continue.', array(
    '@clickhere' => 'javascript:window.close();',
  ));
}
