README for MyEffecto for Drupal 7

MyEffecto 7.x-1.x
=================================

INTRODUCTION
------------
Getting customized and interactive feedback for your blog.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
 * In Configuration Tab -> Web Services, check-out for MyEffecto.
 * Create new account and configure your desired set.

FAQ
---
Q: Is MyEffecto FREE to use?
A: Yes. It is free.

Q: Can I change the MyEffecto Emoticon Set at any point in time?
A: Absolutely. It is completely based on the choice of the author and publisher.
