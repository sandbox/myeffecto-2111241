<?php

/**
 * @file
 * Main file consisting all plugin related code.
 */

$_myeffecto_shortname = NULL;
if (isset($_myeffecto_shortname)) {
  $_myeffecto_shortname = $_GET['shortname'];
}

$_myeffecto_post_id = NULL;
if (isset($_GET['postID'])) {
  $_myeffecto_post_id = $_GET['postID'];
}

$data = NULL;
if (isset($_POST['dataToSend'])) {
  $data = $_POST['dataToSend'];
}

$eff_shortname = NULL;
if (isset($_POST['eff_shortname'])) {
  $eff_shortname = $_POST['eff_shortname'];
}

$post_id = NULL;
if (isset($_GET['postID'])) {
  $post_id = $_GET['postID'];
}

/**
 * Get host of iframe source.
 */
function myeffecto_getHost() {
  $host_string = "http://www.myeffecto.com";
  return $host_string;
}

/**
 * Add script for plugin configuration.
 */
function myeffecto_configurationScript($shortname, $global_post_id) {
  $host_string = myeffecto_getHost();
  return '<script>
				var shortname = "' . $shortname . '";
				var effecto_identifier = "' . $global_post_id . '";

				function save(shortname) {
					if (shortname == null || shortname === "" || shortname === "undefined") {
						ifrm.contentWindow.postMessage("Save#~#drupal","' . $host_string . '");
					} else {
						ifrm.contentWindow.postMessage("Save#~#delete#~#"+shortname+"#~#"+effecto_identifier,"' . $host_string . '");
						shortname = "";
						effecto_identifier = "";
					}
				}

				function receiveMessage(event) {
					var rcvdMsg = event.data;
					var msg = rcvdMsg.split("#~#");

					if (msg[0] == "save") {
						postIframeCode(msg[1]);
						jQuery(\'#load\').show();
					} else if(msg[0] == "loggedIn") {
						afterLoginSuccess();
					} else if (msg[0] == "error") {
						alert("Error occurred");
					} else if (msg[0] == "pluginLoggedIn") {
						showButtonCode(shortname);
					} else if (msg[0] == "validated") {
						jQuery(\'#load\').show();
					} /*else if(msg[0] == "apiKey") {
						addKey(msg[1]);
					}*/
				}

				function postIframeCode(rcvdMsg) {
					var dataToSend = { "insert":"true", "data" : rcvdMsg};
					var test = JSON.parse(rcvdMsg);
					jQuery("#dataToSend").val(test.embedCode);
					jQuery("#eff_shortname").val(test.shortName);
					jQuery(\'#submitForm\').submit();
				}

				function showButtonCode(shortname) {
					jQuery(\'#generate\').remove();
					if (shortname === null) {
					    shortname="";
						jQuery(\'#effectoFrame\').after(jQuery(\'<center><h3><input type="button" id="generate"  value="Apply Emotion Set" style="border-radius: 10px 10px 10px 10px; width: 300px; font-size: 25px; color: blanchedalmond; background-color: royalblue;" /></h3></center>\'));

					} else {
						jQuery(\'#effectoFrame\').after(jQuery(\'<center><h3><input type="button" id="generate" value="Apply Emotion Set" style="border-radius: 10px 10px 10px 10px; width: 300px; font-size: 25px; color: blanchedalmond; background-color: royalblue;"  /></h3></center>\'));
					}

					jQuery("#generate").click(function(){
						save(shortname);
					});
				}

				function afterLoginSuccess() {
					jQuery(\'#effectoFrame\').parent().prepend(jQuery(\'<input type="button" id="generate" onclick="save("")" value="Generate Plugin" class="button-primary"/>\'));
					ifrm.setAttribute("src", "' . $host_string . '/confgEmoji?outside=true");
				}';
}

/**
 * Show iframe for first time users.
 */
function myeffecto_echoFirstUserScript() {
  $host_string = myeffecto_getHost();
  global $_myeffecto_shortname;
  global $_myeffecto_post_id;
  $effscript = myeffecto_configurationScript($_myeffecto_shortname, $_myeffecto_post_id);
  return $effscript . ' var ifrm = null;
				window.onload=function(){
					ifrm = document.getElementById("effectoFrame");
					ifrm.setAttribute("src", "' . $host_string . '/register?callback=confgEmoji&outside=true");
					ifrm.setAttribute("frameborder","0");
					ifrm.setAttribute("allowtransparency","true");

					ifrm.style.width = "100%";
					ifrm.style.height = "465";
					window.addEventListener("message", receiveMessage, false);
				};
			</script>
			<div id="load" style="display:none;"></div>
			<iframe id="effectoFrame" src ="' . $host_string . '/register?callback=confgEmoji&outside=true" width="100%" height="465">';
}

/**
 * Show js alert(For debug purpose only).
 */
function myeffecto_alert($text) {
  echo "<script>alert('" . $text . "')</script>";
}

/**
 * Show default configured plugin which will appear on all posts.
 */
function myeffecto_allSetCode($all_post_code, $get_post_title) {
  $all_post_code = str_replace("var effectoPreview=''", "var effectoPreview='true'", $all_post_code);
  $url         = $_SERVER['REQUEST_URI'];
  $get_post_id   = 0;
  $all_post_code = str_replace("var effectoPostId=''", "var effectoPostId='" . $get_post_id . "'", $all_post_code);
  return '<h2>
				<center>
					Your default emotion set is 
				</center>
			</h2> ' . $all_post_code . ' <h2>
				<center>
					<a style="cursor:pointer;" href="' . $url . '?pluginType=defaultEdit&postURL=' . $url . '" title="Default emotion set appears on all posts.">Change your default emotion set </a>
				</center>
			</h2>';
  // myeffecto_showEffModal();
}

/**
 * Confirmation modal if user is changing defaul set.
 */
function myeffecto_showEffModal() {
  echo '<div id="effecto-confirm" title="Change emotion Set?" style="display : none;">
				<p><span class="" style="float: left; margin: 0 7px 20px 0;">Changing your set will erase your current emotion set data. <br/><br/> Do you want to continue?</span></p>
			</div>

			<script type="text/javascript">
				window.onload=function() {
					jQuery(".effectoConfig").click(function(e) {
						e.preventDefault();
						var targetUrl = jQuery(this).attr("effectohref");
						jQuery( "#effecto-confirm" ).dialog({
							resizable: false,
							height:220,
							modal: false,
							buttons: {
								Ok: function() {
								   window.location.href = targetUrl;
								  //return true;
								},
								Cancel: function() {
								  jQuery( this ).dialog( "close" );
								}
							}
						});
						return false;
					});
				};
			</script>';
}

/**
 * Get embed_code and shortname by post_id.
 */
function myeffecto_getPluginDetails($post_id) {
  $result = db_query('SELECT embedCode,shortname FROM {myeffectodata} WHERE postID = :pid', array(
    ':pid' => $post_id,
  ));
  return $result;
}

/**
 * Get shortname by post_id.
 */
function myeffecto_getShortnameByPostID($post_id) {
  $result = db_query('SELECT shortname FROM {myeffectodata} WHERE postID = :pid', array(
    ':pid' => $post_id,
  ));
  return $result;
}

/**
 * Insert function for embed code insert.
 */
function myeffecto_insertInDb($user_id, $api_key, $code, $post_id, $eff_shortname) {
  $code    = stripcslashes($code);
  $db_name = 'myeffectodata';
  if (myeffecto_getEmbedCodeByPostID($post_id)) {
    myeffecto_updateEmbedCode($code, $post_id, $eff_shortname);
  }
  else {
    $data = array(
      'userid' => $user_id,
      'apiKey' => $api_key,
      'embedCode' => $code,
      'postID' => $post_id,
      'shortname' => $eff_shortname,
    );
    drupal_write_record($db_name, $data);
  }
}

/**
 * Get embed_code by post_id.
 */
function myeffecto_getEmbedCodeByPostID($post_id) {
  if (isset($post_id)) {
    // embedCode FROM $table_name WHERE postID=".$post_id
    return db_query('SELECT embedCode FROM {myeffectodata} WHERE postID = :pid', array(
      ':pid' => $post_id,
    ))->fetchField();
    // WHERE n.uid = :uid', array(':uid' => $uid));
  }
  return NULL;
}

/**
 * Update embed_code and shortname by post_id.
 */
function myeffecto_updateEmbedCode($data, $post_id, $shortname) {
  db_update('myeffectodata')->fields(array(
    'embedCode' => $data,
    'shortname' => $shortname,
  ))->condition('postID', $post_id)->execute();
}
